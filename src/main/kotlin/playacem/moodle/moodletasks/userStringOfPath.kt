package playacem.moodle.moodletasks

import java.io.File
import java.nio.file.Path

/*
We only care about the USERNAME part
Path format:
optional-bla/USERNAME_some-random-text_bla_/FILE-NAME.zip
    split("_")[0]
optional-bla/USERNAME
    split(File.separator, "/").last()
USERNAME
*/
/**
 * Tries to extract an user from the default moodle file structure [path]
 * Returns null if not possible
 */
fun userStringOfPath(path: Path): String? {

	val pathString = path.toString()
	if ("_" !in pathString) {
		// no underscore no useful name possible
		return null
	}

	val elements = pathString.split("_")[0].split(File.separator, "/")
	return elements.lastOrNull()?.ifBlank { null }
}
