package playacem.moodle.moodletasks

import playacem.moodle.api.ZipProvider
import playacem.moodle.filesOf
import java.nio.file.Path

class MoodleZipProvider : ZipProvider {
	override fun getZips(folder: Path): List<Path> {
		// Ignore sub directories
		val matchZip = folder.fileSystem.getPathMatcher("glob:**.zip")
		return filesOf(folder, currentDirOnly = true)
			.asSequence()
			.filter(matchZip::matches)
			.toList()
	}
}
