package playacem.moodle.moodletasks

import java.nio.file.Path

/**
 * Holds all statistics related to user input
 */
object Statistics {
	private val targetToStatus: MutableMap<String, Status> = mutableMapOf()
	var enabled = false

	fun reset() = targetToStatus.clear()

	private fun updateStatus(newStatus: Status, targets: List<String>) {
		if (!enabled) {
			return
		}

		for (target in targets) {
			targetToStatus[target] = newStatus
		}
	}

	fun init(target: String) = init(listOf(target))

	fun init(targets: List<String>) = updateStatus(
		Status.NO_INPUT_DETECTED,
		targets
	)

	fun noZipDetected(target: String) = noZipDetected(listOf(target))

	fun noZipDetected(targets: List<String>) =
		updateStatus(Status.NO_ZIP_DETECTED, targets)

	fun noZipDetected(path: Path) {
		val userString: String? = userStringOfPath(path)
		if (userString != null) {
			noZipDetected(listOf(userString))
		}
	}

	fun noEclipseProject(target: String) = noEclipseProject(listOf(target))

	fun noEclipseProject(targets: List<String>) = updateStatus(
		Status.NO_ECLIPSE_PROJECT,
		targets
	)

	fun valid(target: String) = valid(listOf(target))

	fun valid(targets: List<String>) =
		updateStatus(Status.VALID, targets)

	fun generateReport(): String? {
		if (!enabled) {
			return null
		}
		val statusToTarget: Map<Status, MutableList<String>> = mutableMapOf(
			Status.NO_INPUT_DETECTED to mutableListOf(),
			Status.NO_ZIP_DETECTED to mutableListOf(),
			Status.NO_ECLIPSE_PROJECT to mutableListOf(),
			Status.VALID to mutableListOf()
		)

		targetToStatus.forEach { (target, status) ->
			statusToTarget[status]?.add(target)
		}

		return buildString {
			statusToTarget.forEach { (status, targetList) ->
				// Do not print category if it is empty
				if (targetList.isNotEmpty()) {
					val header = "=== ${status.categoryName} ==="
					appendLine(
						"""
                        |
                        |$header
                        |
                        |${status.description}
                        |
                        |${"=".repeat(header.length)}"""
							.trimMargin("|")
					)
					targetList.forEach {
						appendLine(it)
					}
				}
			}
		}
	}


}

enum class Status(val categoryName: String, val description: String) {
	NO_INPUT_DETECTED("Undetected users", "Users that where not detected in the input zip files."),
	NO_ZIP_DETECTED("No Zip detected", "Users that handed in a non-zip file."),
	NO_ECLIPSE_PROJECT(
		"No Eclipse project detected", """Users that handed in a zip file without a .project file.
     (Invalid Eclipse project)"""
	),
	VALID("Valid projects", "Users that appear to have handed everything in correctly.")
}



