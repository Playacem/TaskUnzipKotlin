package playacem.moodle.moodletasks

import org.w3c.dom.Document
import org.w3c.dom.Node
import playacem.moodle.extensions.notExists
import playacem.moodle.extensions.toPath
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

private val xmlParser: DocumentBuilder by lazy { DocumentBuilderFactory.newInstance().newDocumentBuilder() }
private val transformer: Transformer by lazy { TransformerFactory.newInstance().newTransformer() }

/**
 * Loads a given filename as an XML file and manipulates its name tag to include the users name
 */
fun changeProjectName(
	project: String,
	userString: String? = userStringOfPath(project.toPath()),
	newImpl: Boolean = false
) {

	if (newImpl) {
		changeProjectName(project.toPath(), userString)
		return
	}

	val namePrefix = userString ?: return

	if (project.toPath().notExists) {
		return
	}

	// 1. Parse XML file
	val docBuilder: DocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
	val doc: Document = docBuilder.parse(File(project))

	// 2. Change name value
	val nameNode: Node = doc.getElementsByTagName("name").item(0)
	if (nameNode.parentNode.nodeName != "projectDescription") {
		// wrong name node
		return
	}
	nameNode.textContent = namePrefix + " - " + nameNode.textContent

	// 3. Write changed XML file to disk
	val transformer: Transformer = TransformerFactory.newInstance().newTransformer()
	val source = DOMSource(doc)
	val result = StreamResult(File(project))
	transformer.transform(source, result)
}

/**
 * Loads a given path as an XML file and manipulates its name tag to include the users name
 */
fun changeProjectName(project: Path, userString: String? = userStringOfPath(project)) {
	fun parseAsDocument(path: Path): Document {
		Files.newInputStream(path, StandardOpenOption.READ).use {
			return xmlParser.parse(it)
		}
	}

	fun writeToDisk(document: Document, path: Path) {
		val source = DOMSource(document)
		Files.newOutputStream(path, StandardOpenOption.WRITE).use {
			transformer.transform(source, StreamResult(it))
		}
	}

	val namePrefix = userString ?: return

	if (project.notExists) {
		return
	}

	// 1. Parse XML file
	val doc = parseAsDocument(project)

	// 2. Change name value
	val nameNode: Node = doc.getElementsByTagName("name").item(0)
	if (nameNode.parentNode.nodeName != "projectDescription") {
		// wrong name node
		return
	}
	nameNode.textContent = namePrefix + " - " + nameNode.textContent

	// 3. Write changed XML file to disk
	writeToDisk(doc, project)
}
