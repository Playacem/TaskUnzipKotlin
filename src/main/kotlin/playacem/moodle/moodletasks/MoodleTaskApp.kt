package playacem.moodle.moodletasks

import playacem.moodle.api.FileEvent
import playacem.moodle.api.Unzipper
import playacem.moodle.api.ZipProvider
import playacem.moodle.cli.*
import playacem.moodle.deleteRecursively
import playacem.moodle.extensions.div
import playacem.moodle.extensions.notExists
import playacem.moodle.filesOf
import playacem.moodle.trimFileName
import java.nio.file.Files
import java.nio.file.Path
import kotlin.streams.asSequence

class MoodleTaskApp : CliArgsConsumer, TypedCliArgsConsumer, ZipProvider {
	private val zipProvider: ZipProvider by lazy { MoodleZipProvider() }

	override fun getZips(folder: Path): List<Path> = zipProvider.getZips(folder)

	override fun consume(args: CmdSettings) {
		consume(args.typed())
	}

	override fun consume(args: TypedCmdSettings) {
		val inputPath = args.inputFolder
		if (inputPath.notExists) {
			println("WARNING: The specified input folder ($inputPath) does not exist!")
		}

		// todo generalize the main method

		Statistics.enabled = args.showStatistics
		verifyStatisticsRequirements(args.list)

		val targetPath = args.targetFolder

		if (args.clean) {
			deleteRecursively(targetPath)
		}

		val zips = getZips(inputPath)
		println("Detected zips: $zips")

		// PHASE 1
		// Extract the relevant files
		zips.forEach { zip: Path ->
			// should result in the task name for default Moodle file names
			val fileName = zip.fileName.toString().split("-")[1]
			val out = targetPath / trimFileName(fileName)
			Unzipper.unzip(zip, output = out, encoding = args.encoding, filesToExtract = createFilter(args.list)) {
				Statistics.noZipDetected(it)
			}
		}

		// PHASE 2
		// Unzip zip files
		fileEventsOf(targetPath, args.encoding).forEach(FileHandler::handleOnlyZips)

		// PHASE 3
		// handle renaming of projects
		fileEventsOf(targetPath, args.encoding).forEach(FileHandler::handleNonZips)

		Statistics.generateReport().printIfPresent()
	}
}

private fun fileEventsOf(path: Path, encoding: String) =
	filesOf(path).map { FileEvent(it, encoding, it.toUserString()) }

private fun verifyStatisticsRequirements(listLocation: Path?) {
	if (listLocation == null) {
		println("INFO: Statistics disabled. No file with names specified.")
		Statistics.enabled = false
	} else {
		if (listLocation.notExists) {
			Statistics.enabled = false
			println("WARNING: Statistics disabled. The supplied file ($listLocation) does not exist!")
		} else {
			Statistics.init(Files.readAllLines(listLocation))
		}
	}
}

private fun createFilter(listLocation: Path?): (Path) -> Boolean {
	val listPath = listLocation ?: return { true }

	if (listPath.notExists) {
		return { true }
	}

	val names = Files.lines(listPath).asSequence().filterNotNull().filter(String::isNotBlank).toList()
	return { path: Path -> path.toUserString() in names }
}

private fun Any?.printIfPresent() {
	if (this != null) {
		println(this)
	}
}

private fun Path.toUserString(): String? {
	return userStringOfPath(this)
}
