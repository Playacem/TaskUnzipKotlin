package playacem.moodle.moodletasks

import playacem.moodle.api.FileEvent
import playacem.moodle.api.FileEventHandler
import playacem.moodle.api.Unzipper
import java.nio.file.FileSystems
import java.nio.file.PathMatcher

/**
 * Chain of Responsibility pattern for handling the different file types we encounter
 */

object FileHandler {
	val defaultHandler = DefaultHandler()

	class DefaultHandler : FileEventHandler {
		override var next: FileEventHandler? = null
		override fun apply(event: FileEvent): Boolean = true
		override fun action(event: FileEvent) {
		}
	}

	class ZipHandler : FileEventHandler {
		//See http://stackoverflow.com/questions/20531247/how-to-check-the-extension-of-a-java-7-path
		private val zipMatcher: PathMatcher = FileSystems.getDefault().getPathMatcher("glob:**.zip")
		override var next: FileEventHandler? =
			defaultHandler

		override fun apply(event: FileEvent): Boolean = zipMatcher.matches(event.path)
		override fun action(event: FileEvent) {
			// Ignore individual files in the statistics as those do not have any names associated with them.
			Unzipper.unzip(pathToZip = event.path, encoding = event.encoding)
			if (event.user != null) {
				Statistics.noEclipseProject(event.user)
			}
		}
	}

	class ProjectFileHandler : FileEventHandler {
		private val projectMatcher: PathMatcher = FileSystems.getDefault().getPathMatcher("glob:**.project")
		override var next: FileEventHandler? =
			defaultHandler

		override fun apply(event: FileEvent): Boolean {
			return projectMatcher.matches(event.path) and !event.path.toString().contains("__MACOS")
		}

		override fun action(event: FileEvent) {
			changeProjectName(event.path.toString())
			if (event.user != null) {
				Statistics.valid(event.user)
			}
		}
	}

	fun handleOnlyZips(event: FileEvent) {
		createChain(ZipHandler()).handle(event)
	}

	fun handleNonZips(event: FileEvent) {
		createChain(ProjectFileHandler())
			.handle(event)
	}

	fun createChain(vararg handler: FileEventHandler): FileEventHandler {
		if (handler.isEmpty()) {
			return DefaultHandler()
		}

		handler.indices
			.filter { it != 0 }
			.forEach { handler[it - 1].next = handler[it] }

		return handler[0]
	}
}
