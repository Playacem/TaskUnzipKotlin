package playacem.moodle

import playacem.moodle.extensions.isDirectory
import playacem.moodle.extensions.list
import playacem.moodle.extensions.notExists
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.util.*
import java.util.regex.Matcher
import kotlin.streams.asSequence

/**
 * File related helper methods
 */

/**
 * Recursively finds all files starting in the given [root] path
 * Based on the following stack overflow answer: http://stackoverflow.com/a/23519358
 */
fun filesOf(root: Path, currentDirOnly: Boolean = false): List<Path> {
	if (root.notExists || !root.isDirectory)
		return emptyList()

	val stack = ArrayDeque<Path>()
	val files = mutableListOf<Path>()

	stack.push(root)
	while (stack.isNotEmpty()) {
		Files.newDirectoryStream(stack.pop()).use { stream ->
			stream.forEach {
				if (it.isDirectory) {
					if (!currentDirOnly) {
						stack.push(it)
					}
				} else {
					files.add(it)
				}
			}
		}
	}

	return files
}

fun filesOfLazy(root: Path, currentDirOnly: Boolean = false): Sequence<Path> {
	if (root.notExists || !root.isDirectory)
		return emptySequence()
	val maxDepth = if (currentDirOnly) 1 else Integer.MAX_VALUE
	return Files.walk(root, maxDepth).asSequence().filterNot { it.isDirectory }
}

/**
 * Recursively deletes all files, folders and sub folders
 */
fun deleteRecursively(root: Path) {
	if (root.notExists)
		return
	when {
		root.isDirectory -> {
			root.list().forEach(::deleteRecursively)
			Files.delete(root)
		}

		else -> Files.delete(root)
	}
}

/**
 * Unifies the given File separator in the [filename] string,
 * removes invalid characters in filenames apart from slashes(/\) and also trims excess whitespace.
 * See [Filename limitations on Wikipedia, specifically NTFS](https://en.wikipedia.org/wiki/Filename#Comparison_of_filename_limitations)
 */
fun trimFileName(filename: String): String {
	/*
	 * \x00-\x1F \x7F - control characters
	 * \x22 - "
	 * \x2A - *
	 * \x3B-\x3F ; - < = > ?
	 * \x7C - |
	 * Also see https://unicode-table.com/
	 */
	return filename.replace("""\s*([\\/])\s*""".toRegex(), Matcher.quoteReplacement(File.separator))
		.replace("""[\x00-\x1F\x7F\x22\x2A\x3B-\x3F\x7C]""".toRegex(), "")
		.trim()
}
