/**
 * Copied straight from [this pull request](https://github.com/Kotlin/kotlinx.support/pull/2)
 */
@file:Suppress("UNUSED", "unused")

package playacem.moodle.extensions


import java.net.URI
import java.nio.file.*
import java.nio.file.attribute.FileTime
import java.nio.file.attribute.UserPrincipal
import java.util.*
import java.util.stream.Collectors
import java.util.stream.Stream

/** Converts a path string to a [Path]. */
fun String.toPath(fileSystem: FileSystem = FileSystems.getDefault()): Path = fileSystem.getPath(this)

/** Converts a [String] to a [Path] */
fun pathOf(path: String, fileSystem: FileSystem = FileSystems.getDefault()): Path = path.toPath(fileSystem)

/** Converts a [URI] to a [Path]. This method uses the default FileSystem if the file scheme is used. */
fun pathOf(uri: URI): Path = Paths.get(uri)

/**
 * Converts multiple [String]s to a [Path].
 *
 * Empty strings are ignored by the underlying implementation.
 */
fun pathOf(vararg paths: String, fileSystem: FileSystem = FileSystems.getDefault()): Path =
	fileSystem.getPath("", *paths)


/**
 * The following extensions implement syntactic sugar for path
 * construction.
 *
 * For example
 *
 *     root.resolve("foo").resolve("bar")
 *
 * can be constructed as
 *
 *     root / "foo" / "bar"
 */
operator fun Path.div(other: String): Path = div(other.toPath())

operator fun Path.div(other: Path): Path = resolve(other)
operator fun String.div(other: String) = div(other.toPath())
operator fun String.div(other: Path) = toPath() / other

/** Returns `false` if a file exists and `true` otherwise. */
val Path.notExists: Boolean get() = Files.notExists(this)

/** Returns `true` if a file exists and `false` otherwise. */
val Path.exists: Boolean get() = Files.exists(this)

/** Tests whether a file can be read. */
val Path.isReadable: Boolean get() = Files.isReadable(this)

/** Tests whether a file can be written to. */
val Path.isWritable: Boolean get() = Files.isWritable(this)

/** Tests whether a file can be executed. */
val Path.isExecutable: Boolean get() = Files.isExecutable(this)

/** Tests whether a path points to a directory. */
val Path.isDirectory: Boolean get() = Files.isDirectory(this)

/** Tests whether a path is a regular file. */
val Path.isRegularFile: Boolean get() = Files.isRegularFile(this)

/** Tests whether a path is a symbolic link. */
val Path.isSymbolicLink: Boolean get() = Files.isSymbolicLink(this)

/**
 * File size in bytes.
 */
val Path.size: Long get() = Files.size(this)

/**
 * Timestamp of the last modification.
 */
var Path.lastModifiedTime: FileTime
	get() = Files.getLastModifiedTime(this)
	set(value) = ignore(Files.setLastModifiedTime(this, value))

/**
 * Owner of this path.
 */
var Path.owner: UserPrincipal
	get() = Files.getOwner(this)
	set(value) = ignore(Files.setOwner(this, value))

/**
 * Returns a list of entries from the directory corresponding to this path.
 *
 * Note that the return type is intentionally different from
 * [Files.list]. Wrapping each [list] call in a try-finally block is
 * too much of a burden.
 */
fun Path.list(): List<Path> {
	val stream: Stream<Path> = Files.list(this)
	stream.use {
		return it.collect(Collectors.toList())
	}
}

/** Walks a file tree starting from this path. */
fun Path.walkFileTree(
	walker: FileVisitor<Path>,
	options: Set<FileVisitOption> = EnumSet.noneOf(FileVisitOption::class.java),
	maxDepth: Int = Integer.MAX_VALUE
) {
	Files.walkFileTree(this, options, maxDepth, walker)
}

/** A function which simply ignores a given [returnValue]. */
@Suppress("UNUSED_PARAMETER")
private fun ignore(returnValue: Any?) = Unit
