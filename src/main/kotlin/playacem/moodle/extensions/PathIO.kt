/**
 * Contains extensions related to Path IO
 */
@file:Suppress("UNUSED")

package playacem.moodle.extensions

import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.StandardCopyOption


/**
 * Opens this path for reading.
 *
 * See [Files.newBufferedReader] for complete documentation.
 */
fun Path.bufferedReader(charset: Charset = StandardCharsets.UTF_8): BufferedReader {
	return Files.newBufferedReader(this, charset)
}

/**
 * Opens this path for writing.
 *
 * See [Files.newBufferedWriter] for complete documentation.
 */
fun Path.bufferedWriter(
	charset: Charset = StandardCharsets.UTF_8,
	vararg options: OpenOption
): BufferedWriter {
	return Files.newBufferedWriter(this, charset, *options)
}

/**
 * Copies all bytes from this stream to a given path.
 *
 * See [Files.copy] for complete documentation.
 */
fun InputStream.copy(target: Path, vararg options: StandardCopyOption): Long = Files.copy(this, target, *options)


/**
 * Copies all bytes from a file to an output stream.
 *
 * See [Files.copy] for complete documentation.
 */
fun Path.copy(outputStream: OutputStream): Long = Files.copy(this, outputStream)

/**
 * Copies a path to a [target] path.
 *
 * See [Files.copy] for complete documentation.
 */
fun Path.copy(target: Path, vararg options: StandardCopyOption): Path = Files.copy(this, target, *options)

/**
 * Moves or renames a path to a [target] path.
 *
 * See [Files.move] for complete documentation.
 */
fun Path.move(target: Path, vararg options: StandardCopyOption): Path = Files.move(this, target, *options)

