package playacem.moodle.cli

interface CliArgsConsumer {
	fun consume(args: CmdSettings)
}

interface TypedCliArgsConsumer {
	fun consume(args: TypedCmdSettings)
}
