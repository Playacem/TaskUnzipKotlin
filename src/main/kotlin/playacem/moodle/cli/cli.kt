package playacem.moodle.cli

import com.beust.jcommander.JCommander
import com.beust.jcommander.Parameter
import picocli.CommandLine
import playacem.moodle.extensions.toPath
import java.nio.file.Path

/**
 * All the possible settings which are settable from the command line.
 * Used by JCommander
 */
interface CmdSettings {
	val inputFolder: String
	val list: String?
	val targetFolder: String
	val encoding: String
	val clean: Boolean
	val showStatistics: Boolean
	val help: Boolean
}

/**
 * All the possible settings which are settable from the command line.
 * Used by PicoCli
 */
interface TypedCmdSettings {
	val inputFolder: Path
	val list: Path?
	val targetFolder: Path
	val encoding: String
	val clean: Boolean
	val showStatistics: Boolean
	val help: Boolean
}

/**
 * This class represents possible command line arguments and their defaults
 * and an instance
 * holds them after they get parsed by JCommander.
 */
private class JCommanderCommandLineArgs : CmdSettings {

	@Parameter(
		names = ["-i", "--in", "--input", "-f", "--folder"],
		description = "The folder to be searched for zip files. " +
			"Defaults to current directory."
	)
	override var inputFolder: String = "."

	@Parameter(
		names = ["-l", "--list"],
		description = "The name of the file to be used in the filtering phase. " +
			"The file should contain one user per line. " +
			"This is required if you want to use --show-stats"
	)
	override var list: String? = null

	@Parameter(
		names = ["-o", "--out", "--output"],
		description = "The folder to be used for the generated output."
	)
	override var targetFolder: String = "output"

	@Parameter(
		names = ["-e", "--encoding"],
		description = "Encoding that is used to treat files."
	)
	override var encoding: String = "UTF-8"

	@Parameter(
		names = ["-c", "--clean"],
		description = "Clean output folder before running."
	)
	override var clean: Boolean = false

	@Parameter(
		names = ["--show-stats"], description = "Display statistics after running the program. " +
			"You need to specify a list of users with --list for this option to work."
	)
	override var showStatistics: Boolean = false

	@Parameter(
		names = ["-h", "--help", "-?", "--usage"],
		description = "Prints this help text", help = true
	)
	override var help: Boolean = false

}

/**
 * This class represents possible command line arguments and their defaults
 * and an instance of this class
 * holds them after they get parsed by PicoCli.
 */
private class PicoCliCommandLineArgs : TypedCmdSettings {

	@set:CommandLine.Option(
		names = ["-i", "--in", "--input", "-f", "--folder"],
		description = ["The folder to be searched for zip files. ",
			"Defaults to current directory."]
	)
	override var inputFolder: Path = ".".toPath()

	@set:CommandLine.Option(
		names = ["-l", "--list"],
		description = ["The name of the file to be used in the filtering phase. ",
			"The file should contain one user per line. ",
			"This is required if you want to use --show-stats"]
	)
	override var list: Path? = null

	@set:CommandLine.Option(
		names = ["-o", "--out", "--output"],
		description = ["The folder to be used for the generated output."]
	)
	override var targetFolder: Path = "output".toPath()

	@set:CommandLine.Option(
		names = ["-e", "--encoding"],
		description = ["Encoding that is used to treat files."]
	)
	override var encoding: String = "UTF-8"

	@set:CommandLine.Option(
		names = ["-c", "--clean"],
		description = ["Clean output folder before running."]
	)
	override var clean: Boolean = false

	@set:CommandLine.Option(
		names = ["--show-stats"],
		description = ["Display statistics after running the program. ",
			"You need to specify a list of users with --list for this option to work."]
	)
	override var showStatistics: Boolean = false

	@set:CommandLine.Option(
		names = ["-h", "--help", "-?", "--usage"],
		description = ["Prints this help text"],
		help = true
	)
	override var help: Boolean = false
}

/**
 * On creation freezes the given Command line values.
 * This enables smart casting for those values as they are not modifiable.
 */
data class Arguments(
	override val inputFolder: String,
	override val list: String?,
	override val targetFolder: String,
	override val encoding: String,
	override val clean: Boolean,
	override val showStatistics: Boolean,
	override val help: Boolean
) : CmdSettings

data class TypedArguments(
	override val inputFolder: Path,
	override val list: Path?,
	override val targetFolder: Path,
	override val encoding: String,
	override val clean: Boolean,
	override val showStatistics: Boolean,
	override val help: Boolean
) : TypedCmdSettings

private fun createArguments(argsParsed: CmdSettings): Arguments {
	return with(argsParsed) {
		Arguments(
			inputFolder,
			list,
			targetFolder,
			encoding,
			clean,
			showStatistics,
			help
		)
	}
}

private fun createTypedArguments(argsParsed: TypedCmdSettings): TypedArguments {
	return with(argsParsed) {
		TypedArguments(
			inputFolder,
			list,
			targetFolder,
			encoding,
			clean,
			showStatistics,
			help
		)
	}
}


fun parseArgs(args: Array<String>): Arguments {
	val argsHolder = JCommanderCommandLineArgs()

	val commander = JCommander(argsHolder)
	commander.parse(*args)
	commander.programName = "TaskUnzipKotlin"

	// print help if no arguments are supplied
	if (args.isEmpty()) {
		argsHolder.help = true
	}

	if (argsHolder.help) {
		commander.usage()
	}

	return createArguments(argsHolder)
}

fun parseArgsTyped(args: Array<String>): TypedArguments {
	val argsHolder = PicoCliCommandLineArgs()
	val commandLine = CommandLine(argsHolder)
	val parseResult: CommandLine.ParseResult = commandLine.parseArgs(*args)


	if (args.isEmpty()) {
		argsHolder.help = true
	}

	if (argsHolder.help) {
		CommandLine.printHelpIfRequested(parseResult)
	}

	return createTypedArguments(argsHolder)
}

fun CmdSettings.typed(): TypedCmdSettings {
	return TypedArguments(
		inputFolder = inputFolder.toPath(),
		list = list?.toPath(),
		targetFolder = targetFolder.toPath(),
		encoding = encoding,
		clean = clean,
		showStatistics = showStatistics,
		help = help
	)
}
