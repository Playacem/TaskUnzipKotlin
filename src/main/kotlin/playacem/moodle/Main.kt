package playacem.moodle


import playacem.moodle.cli.parseArgsTyped
import playacem.moodle.moodletasks.MoodleTaskApp

fun main(args: Array<String>) {
	val cliArgs = parseArgsTyped(args)
	if (cliArgs.help) {
		return
	}

	val moodleApp = MoodleTaskApp()
	moodleApp.consume(cliArgs)
}
