package playacem.moodle.api

import java.nio.file.Path

interface ZipProvider {
	fun getZips(folder: Path): List<Path>
}
