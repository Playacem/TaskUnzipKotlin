package playacem.moodle.api

import playacem.moodle.extensions.copy
import playacem.moodle.extensions.notExists
import playacem.moodle.extensions.toPath
import playacem.moodle.filesOf
import playacem.moodle.trimFileName
import java.net.URI
import java.nio.file.*

// all junk macOSFiles to be ignored while extracting
private val macOSIgnoredFiles = """
### macOS ###
*.DS_Store
.AppleDouble
.LSOverride

# Icon must end with two \r
Icon\r\r
# Thumbnails
._*
# Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent
# Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk
""".lines().filter { !it.startsWith("#") and it.isNotBlank() }

/**
 * Contains the functions related to data manipulation
 */
object Unzipper {
	private val acceptEveryPath: (Path) -> Boolean = { true }

	/**
	 * Unzips entries in a given zip file with an optional filter
	 */
	fun unzip(
		pathToZip: Path,
		output: Path = pathToZip.parent,
		encoding: String = "UTF-8",
		filesToExtract: (Path) -> Boolean = acceptEveryPath,
		beforeEachFile: (Path) -> Unit = {}
	) {
		// based on http://www.tutego.de/blog/javainsel/2012/07/how-to-put-files-in-a-zip-file-with-nio-2/
		// and http://docs.oracle.com/javase/7/docs/technotes/guides/io/fsp/zipfilesystemprovider.html
		val zipUri = URI.create("jar:" + pathToZip.toUri())
		val env = mapOf("create" to "false", "encoding" to encoding)
		FileSystems.newFileSystem(zipUri, env).use { zipFs ->
			filesOf(zipFs.getPath("/"))
				.asSequence()
				.filter { filesToExtract(it) }
				.filter { filterMacOS(zipFs, it) }
				.toList()
				.forEach { fileInZip: Path ->
					beforeEachFile(fileInZip)
					// resolve cares about FileSystemProviders
					val localizedPath = pathTransform(output.fileSystem, fileInZip)
					// "." enables us to treat the absolute path we got from the zip file system as a relative path
					// normalize removes the "." afterwards
					val externalPath = output.resolve(".$localizedPath").normalize()

					if (externalPath.notExists) {
						Files.createDirectories(externalPath)
					}
					// copy does not care about Providers
					fileInZip.copy(externalPath, StandardCopyOption.REPLACE_EXISTING)
				}
		}
	}

	/**
	 * Filters a given [path] if it is to be ignored.
	 * This filter ignores typical macOS specific files.
	 * @return true if allowed, false otherwise
	 */
	private fun filterMacOS(fs: FileSystem, path: Path): Boolean {
		val matcherList = macOSIgnoredFiles.map { fs.getPathMatcher("glob:*$it") }
		return matcherList.none { it.matches(path) }
	}

	/**
	 * Transforms a given [path] from its filesystem to the given [fs].
	 * Helps avoid ProviderMismatchExceptions that get thrown by resolve as resolve cares about FileSystemProviders
	 * Also converts between the different file separators.
	 * @return the transformed path
	 *
	 * see StackOverflow
	 * [http://stackoverflow.com/questions/22611919/]
	 */
	private fun pathTransform(fs: FileSystem, path: Path): Path {
		var result: Path = if (path.isAbsolute) fs.separator.toPath() else "".toPath()
		for (component in path) {
			result = result.resolve(trimFileName(component.fileName.toString()))
		}
		return result
	}

}
