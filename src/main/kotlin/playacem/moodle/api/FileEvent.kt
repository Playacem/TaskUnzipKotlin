package playacem.moodle.api

import java.nio.file.Path

data class FileEvent(val path: Path, val encoding: String = "UTF-8", val user: String? = null)
