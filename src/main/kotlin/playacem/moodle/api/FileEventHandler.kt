package playacem.moodle.api

interface FileEventHandler {
	var next: FileEventHandler?
	fun apply(event: FileEvent): Boolean
	fun action(event: FileEvent)
	fun handle(event: FileEvent) {
		when {
			apply(event) -> action(event)
			else -> next?.handle(event)
		}
	}
}
