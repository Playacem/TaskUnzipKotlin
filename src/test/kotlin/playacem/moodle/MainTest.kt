package playacem.moodle

import org.testng.annotations.Test

@Test
class MainTest {

	fun smokeTest() {
		main(arrayOf("--help"))
	}

	fun triggerInputNotFound() {
		main(arrayOf("-i", "fake"))
	}

	fun triggerClean() {
		main(arrayOf("-o", "fake", "-c"))
	}

	fun statisticsListNotFound() {
		main(arrayOf("-l", "fake", "-c"))
	}
}
