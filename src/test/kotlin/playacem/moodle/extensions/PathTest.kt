package playacem.moodle.extensions

import org.testng.Assert.assertEquals
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.extensions.PathLib.invalidPath
import playacem.moodle.extensions.PathLib.validPath
import playacem.moodle.util.DataArray
import playacem.moodle.util.and
import playacem.moodle.util.unaryPlus
import java.net.URI
import java.nio.file.*
import java.nio.file.attribute.FileTime
import java.nio.file.attribute.UserPrincipal

@Test
class PathTest {
	private fun pathExtensionTestData(path: Path): DataArray {
		return arrayOf(
			+path.notExists and Files.notExists(path),
			+path.exists and Files.exists(path),
			+path.isReadable and Files.isReadable(path),
			+path.isWritable and Files.isWritable(path),
			+path.isExecutable and Files.isExecutable(path),
			+path.isDirectory and Files.isDirectory(path),
			+path.isRegularFile and Files.isRegularFile(path),
			+path.isSymbolicLink and Files.isSymbolicLink(path)
		)
	}

	@DataProvider(name = "boolProp")
	fun booleanPathExtensionPropertiesData(): DataArray {
		val path = pathOf("/", "this", "is", "a", "test")
		return pathExtensionTestData(path) + pathExtensionTestData(validPath)
	}

	@Test(dataProvider = "boolProp")
	fun booleanPathExtensionProperties(extension: Boolean, original: Boolean) {
		assertEquals(extension, original)
	}

	@DataProvider(name = "pathOf")
	fun pathOfData(): DataArray {
		return arrayOf(
			+"",
			+"hi",
			+"data with spaces",
			+"data/with/slashes",
			+"long 1"
		)
	}

	@Test(dataProvider = "pathOf")
	fun pathOfSingle(value: String) {
		val actual = pathOf(value)
		val expectedPaths = Paths.get(value)
		val expectedStringExtension = value.toPath()
		assertEquals(actual, expectedPaths)
		assertEquals(actual, expectedStringExtension)
	}

	@DataProvider(name = "pathOf-Uri")
	fun pathOfUriData(): DataArray {
		return arrayOf(
			+URI("file:///test.txt"),
			+URI("file:///space%20test.txt")
		)
	}

	@Test(dataProvider = "pathOf-Uri")
	fun pathOfUri(value: URI) {
		val actual = pathOf(value)
		val expected = Paths.get(value)
		assertEquals(actual, expected)
	}

	@DataProvider(name = "pathOf-Varargs")
	fun pathOfVarargsData(): DataArray {
		return arrayOf(
			+"" and "bu",
			+"data" and "meh",
			+"hi world" and "bye",
			+"random" and "stuff",
			+"this" and "is" and "random",
			+"data" and "with" and "4" and "elements",
			+"data" and "with" and "5" and "elements" and "1",
			+"data" and "with" and "6" and "elements" and "1" and "2",
			+"data" and "with" and "7" and "elements" and "1" and "2" and "3",
			+"data" and "with" and "8" and "elements" and "1" and "2" and "3" and "4",
			+"data" and "with" and "9" and "elements" and "1" and "2" and "3" and "4" and "5",
			+"data" and "with" and "10" and "elements" and "1" and "2" and "3" and "4" and "5" and "6",
			+"data" and "with" and "11" and "elements" and "1" and "2" and "3" and "4" and "5" and "6" and "7"
		)
	}

	@Test(dataProvider = "pathOf-Varargs")
	fun pathOfVarargs(one: String, rest: Array<String>) {
		val actual = pathOf(one, *rest)
		val expected = Paths.get("", one, *rest)
		assertEquals(actual, expected)
	}

	@Test(expectedExceptions = [java.nio.file.FileSystemException::class])
	fun sizeExtensionException() {
		val extension = invalidPath.size
		val original = Files.size(invalidPath)
		assertEquals(extension, original)
	}

	@Test
	fun sizeExtension() {
		val actual = validPath.size
		val expected = Files.size(validPath)
		assertEquals(actual, expected)
	}

	@Test
	fun lastModifiedTimeExtensionGetter() {
		// getter
		val actual = validPath.lastModifiedTime
		val expected = Files.getLastModifiedTime(validPath)
		assertEquals(actual, expected)
	}

	@Test
	fun lastModifiedTimeExtensionSetter() {
		val tmp = Files.createTempFile("extension-lastModified-setter", "tmp")
		val original = Files.getLastModifiedTime(tmp)
		// setter
		val newTime = FileTime.fromMillis(400_000)
		tmp.lastModifiedTime = newTime
		assertEquals(tmp.lastModifiedTime, newTime)

		// reset
		tmp.lastModifiedTime = original
		Files.delete(tmp)
	}

	@Test
	fun ownerExtensionGetter() {
		val actual = validPath.owner
		val expected = Files.getOwner(validPath)
		assertEquals(actual, expected)
	}

	@Test(enabled = true)
	fun ownerExtensionSetter() {
		// Access rights can be funky if we don't let the program create the file
		// So we create a temporary file just for this test
		val tmp = Files.createTempFile("extension-owner-setter", "tmp")
		val original: UserPrincipal = Files.getOwner(tmp)

		tmp.owner = original
		assertEquals(tmp.owner, original)

		//reset
		tmp.owner = original
		Files.delete(tmp)
	}
}
