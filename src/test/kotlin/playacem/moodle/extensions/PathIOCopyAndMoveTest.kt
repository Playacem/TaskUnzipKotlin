package playacem.moodle.extensions

import org.testng.Assert.assertFalse
import org.testng.Assert.assertTrue
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import playacem.moodle.extensions.PathLib.confirmCopyResult
import playacem.moodle.extensions.PathLib.toBeRead
import playacem.moodle.extensions.PathLib.toBeReadBackup
import playacem.moodle.extensions.PathLib.toBeWrittenTo
import java.nio.file.Files
import java.nio.file.StandardOpenOption


@Test(enabled = true)
class PathIOCopyAndMoveTest {

	@BeforeMethod
	fun setUp() {
		Files.copy(toBeRead, toBeReadBackup)
	}

	@AfterMethod
	fun cleanUp() {
		Files.deleteIfExists(toBeRead)
		Files.delete(toBeWrittenTo)

		Files.move(toBeReadBackup, toBeRead)
	}

	fun copyFromStreamToPath() {
		Files.newInputStream(toBeRead, StandardOpenOption.READ).use {
			it.copy(toBeWrittenTo)
		}
		confirmCopyResult(toBeWrittenTo)
	}

	fun copyFromPathToStream() {
		Files.newOutputStream(toBeWrittenTo, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW).use {
			toBeRead.copy(it)
		}
		confirmCopyResult(toBeWrittenTo)
	}

	fun copyFromPathToPath() {
		toBeRead.copy(toBeWrittenTo)
		confirmCopyResult(toBeWrittenTo)
	}

	fun move() {
		toBeRead.move(toBeWrittenTo)

		assertFalse(toBeRead.exists)
		assertTrue(toBeWrittenTo.exists)
	}
}
