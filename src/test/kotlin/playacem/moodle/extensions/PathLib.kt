@file:Suppress("UNUSED")

package playacem.moodle.extensions

import org.testng.Assert.assertEquals
import org.testng.Assert.assertTrue
import playacem.moodle.util.ResourceFolder
import java.nio.file.Path

/**
 * Contains all non-local paths for this package
 */

object PathLib {
	private val basePath = ResourceFolder.SELF / "PathKt"
	private val readerFolderPath = basePath / "reader"
	private val writerFolderPath = basePath / "writer"
	val validPath = basePath / "valid-file.txt"

	val invalidPath = pathOf("/", "this", "is a", "testing", "facility")

	val toBeRead = readerFolderPath / "file-with-content.txt"
	val toBeReadBackup = basePath / "file.bak"
	val toBeWrittenTo = writerFolderPath / "write-target.txt"

	fun confirmCopyResult(path: Path) {
		assertTrue(path.exists)
		path.bufferedReader().useLines {
			val list = it.toList()
			assertEquals(list.size, 1)
			assertTrue("File content" in list)
		}
	}
}
