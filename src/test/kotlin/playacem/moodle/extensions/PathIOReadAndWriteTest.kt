package playacem.moodle.extensions

import org.testng.Assert.*
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import playacem.moodle.extensions.PathLib.toBeRead
import playacem.moodle.extensions.PathLib.toBeReadBackup
import playacem.moodle.extensions.PathLib.toBeWrittenTo
import java.io.BufferedWriter
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.Files
import java.nio.file.OpenOption
import java.nio.file.StandardOpenOption.*


@Test(enabled = true)
class PathIOReadAndWriteTest {

	@BeforeMethod
	fun setUp() {
		Files.copy(toBeRead, toBeReadBackup)
		Files.copy(toBeRead, toBeWrittenTo)
	}

	@AfterMethod
	fun cleanUp() {
		Files.deleteIfExists(toBeRead)
		Files.deleteIfExists(toBeWrittenTo)
		// restore one time backup
		Files.move(toBeReadBackup, toBeRead)
	}

	fun bufferedReader() {
		val actual = toBeRead.bufferedReader().useLines { it.toList() }
		val expected = Files.newBufferedReader(toBeRead, UTF_8).useLines { it.toList() }
		assertEquals(actual, expected)
	}

	fun bufferedWriter() {
		val originalLines = Files.newBufferedReader(toBeWrittenTo).useLines { it.toList() }
		toBeWrittenTo.bufferedWriter(
			UTF_8,
			APPEND, WRITE
		).use { writeToWriter(it, "actual") }

		val afterActual = Files.newBufferedReader(toBeWrittenTo).useLines { it.toList() }
		Files.newBufferedWriter(
			toBeWrittenTo, UTF_8,
			APPEND, WRITE
		).use { writeToWriter(it, "expected") }

		val afterExpected = Files.newBufferedReader(toBeWrittenTo).useLines { it.toList() }

		assertNotEquals(originalLines, afterActual)
		assertNotEquals(originalLines, afterExpected)
		assertNotEquals(afterActual, afterExpected)


		originalLines.forEach {
			assertTrue(it in afterActual)
			assertTrue(it in afterExpected)
		}

		afterActual.forEach { assertTrue(it in afterExpected) }
	}

	fun bufferedWriterWithDifferentCharset() {
		val tmp = Files.createTempFile("test", "got")

		tmp.bufferedWriter(options = arrayOf<OpenOption>(CREATE, TRUNCATE_EXISTING)).use { writeToWriter(it, "hi") }
		val actual = tmp.bufferedReader().useLines { it.toList() }
		val expected = listOf("hi")

		assertEquals(actual, expected)
		Files.deleteIfExists(tmp)
	}

	private fun writeToWriter(writer: BufferedWriter, text: String) {
		writer.write(text)
		writer.newLine()
		writer.flush()
	}
}
