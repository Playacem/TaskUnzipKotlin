package playacem.moodle.extensions

import org.testng.Assert.assertEquals
import org.testng.annotations.Test
import playacem.moodle.extensions.PathLib.validPath
import java.io.IOException
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes
import java.util.*

class FileWalkerExtensionTest {
	data class FileVisitorStats(
		var preDir: Int = 0,
		var postDir: Int = 0,
		var fileVisited: Int = 0,
		var fileVisitFailed: Int = 0
	)

	class FileVisitorImpl : FileVisitor<Path> {
		val stats: FileVisitorStats =
			FileVisitorStats()

		override fun postVisitDirectory(dir: Path?, exc: IOException?): FileVisitResult {
			stats.postDir++
			return FileVisitResult.CONTINUE
		}

		override fun visitFileFailed(file: Path?, exc: IOException?): FileVisitResult {
			stats.fileVisitFailed++
			return FileVisitResult.CONTINUE
		}

		override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?): FileVisitResult {
			stats.preDir++
			return FileVisitResult.CONTINUE
		}

		override fun visitFile(file: Path?, attrs: BasicFileAttributes?): FileVisitResult {
			stats.fileVisited++
			return FileVisitResult.CONTINUE
		}
	}

	@Test
	fun fileWalkerExtension() {
		val actualVisitor = FileVisitorImpl()
		val expectedVisitor = FileVisitorImpl()
		validPath.walkFileTree(actualVisitor)
		Files.walkFileTree(validPath, EnumSet.noneOf(FileVisitOption::class.java), Integer.MAX_VALUE, expectedVisitor)
		assertEquals(actualVisitor.stats, expectedVisitor.stats)
	}
}
