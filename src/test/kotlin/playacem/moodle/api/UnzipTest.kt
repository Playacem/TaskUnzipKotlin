package playacem.moodle.api

import org.testng.Assert.assertTrue
import org.testng.annotations.Test
import playacem.moodle.deleteRecursively
import playacem.moodle.extensions.div
import playacem.moodle.extensions.exists
import playacem.moodle.extensions.isDirectory
import playacem.moodle.extensions.notExists
import playacem.moodle.filesOf
import playacem.moodle.util.ResourceFolder
import java.nio.file.Files

@Test
class UnzipTest {
	private val pathToZip = ResourceFolder.SELF / "moodle-export" / "Dummy Moodle-Task 1-0000000.zip"

	fun unzipWithDefaults() {
		val tmp = Files.createTempDirectory("unzip-test")
		Unzipper.unzip(pathToZip, output = tmp)
		assertTrue(tmp.exists)
		assertTrue(tmp.isDirectory)
		assertTrue(filesOf(tmp).isNotEmpty())

		deleteRecursively(tmp)
		assertTrue(tmp.notExists)
	}
}
