package playacem.moodle

import org.testng.Assert.assertEquals
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.extensions.div
import playacem.moodle.extensions.pathOf
import playacem.moodle.util.DataArray
import playacem.moodle.util.ResourceFolder
import playacem.moodle.util.and
import java.nio.file.Path

@Test
class FilesOfTest {

	private val nonExistingPath = pathOf("/this/does/not/exist")
	private val pathToFile = ResourceFolder.SELF / "filesOf" / "placeholder.txt"
	private val dir = ResourceFolder.SELF / "filesOf"

	@DataProvider(name = "filesOf-Data-default")
	fun subDirData(): DataArray {
		return arrayOf(
			nonExistingPath and emptySet<Path>(),
			pathToFile and emptySet<Path>(),
			dir and setOf(
				dir / "placeholder.txt",
				dir / "sub-dir" / "sub-dir-file.txt"
			)
		)
	}

	private fun currentDirData(): DataArray {
		return arrayOf(
			nonExistingPath and emptySet<Path>() and true,
			pathToFile and emptySet<Path>() and true,
			dir and setOf(dir / "placeholder.txt") and true
		)
	}

	@DataProvider(name = "filesOf-Data")
	fun createFilesOfData(): DataArray {
		val withFlag = subDirData().map { arrayOf(*it, false) }.toTypedArray()
		return withFlag + currentDirData()
	}

	@Test(dataProvider = "filesOf-Data")
	fun eager(path: Path, expected: Set<Path>, currentDirOnly: Boolean) {
		val actual = filesOf(path, currentDirOnly).toSet()
		assertEquals(
			actual,
			expected,
			"eager failed! currentDirOnly: $currentDirOnly actual: $actual, expected: $expected"
		)
	}

	@Test(dataProvider = "filesOf-Data")
	fun lazy(path: Path, expected: Set<Path>, currentDirOnly: Boolean) {
		val actual = filesOfLazy(path, currentDirOnly).toSet()
		assertEquals(
			actual,
			expected,
			"lazy failed! currentDirOnly: $currentDirOnly actual: $actual, expected: $expected"
		)
	}

	@Test(dataProvider = "filesOf-Data-default")
	fun eagerDefault(path: Path, expected: Set<Path>) {
		val actual = filesOf(path).toSet()
		assertEquals(actual, expected, "eager default failed! actual: $actual, expected: $expected")
	}

	@Test(dataProvider = "filesOf-Data-default")
	fun lazyDefault(path: Path, expected: Set<Path>) {
		val actual = filesOfLazy(path).toSet()
		assertEquals(actual, expected, "lazy default failed! actual: $actual, expected: $expected")
	}
}
