package playacem.moodle

import org.testng.Assert.assertEquals
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import playacem.moodle.extensions.*
import playacem.moodle.util.ResourceFolder
import java.nio.file.Files
import java.nio.file.Path

@Test
class DeleteRecursivelyTest {
	private val baseDir = ResourceFolder.SELF / "deleteRecursively"

	private val tmp = baseDir / "tmp"
	private val start = tmp / "start"

	private val tmpTxt = tmp / "inside-tmp.txt"
	private val startTxt = start / "inside-start.txt"
	private val startTxt2 = start / "inside-start2.txt"

	private val files = listOf(tmpTxt, startTxt, startTxt2)
	private val dirs = listOf(tmp, start)
	private val filesAndDirs = files + dirs

	@BeforeMethod
	fun setup() {
		Files.createDirectories(start)
		files.forEach { if (it.notExists) Files.createFile(it) }
	}

	@AfterMethod
	fun teardown() {
		deleteRecursively(tmp)
	}

	private fun testSetup() {
		files.map(Path::isRegularFile).allTrue()
		dirs.map(Path::isDirectory).allTrue()
		filesAndDirs.map(Path::exists).allTrue()
	}

	@Test
	fun deleteRecursivelyInsideTmp() {
		testSetup()

		deleteRecursively(start)

		val deletedFiles = listOf(startTxt, startTxt2)
		val deletedDirs = listOf(start)
		val deletedFilesAndDirs = deletedFiles + deletedDirs

		val existingFilesAndDirs = filesAndDirs - deletedFilesAndDirs.toSet()

		deletedFilesAndDirs.map(Path::exists).allFalse()
		existingFilesAndDirs.map(Path::exists).allTrue()
	}

	@Test
	fun deleteRecursivelyTmp() {
		testSetup()

		deleteRecursively(tmp)

		files.map(Path::exists).allFalse()
		dirs.map(Path::exists).allFalse()
	}

	private fun allBool(bool: Boolean): (List<*>) -> Unit =
		{ values -> values.forEach { assertEquals(it == true, bool) } }

	private fun List<*>.allTrue() = allBool(true)(this)
	private fun List<*>.allFalse() = allBool(false)(this)
}
