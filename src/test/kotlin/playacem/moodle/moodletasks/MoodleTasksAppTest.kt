package playacem.moodle.moodletasks

import org.testng.annotations.Test
import playacem.moodle.cli.Arguments
import playacem.moodle.cli.typed

@Test
class MoodleTasksAppTest {
	private val defaultArgs = Arguments(
		inputFolder = ".",
		list = null,
		targetFolder = "output",
		encoding = "UTF-8",
		clean = false,
		showStatistics = false,
		help = false
	)

	fun jCommanderPath() {
		val subject = MoodleTaskApp()
		subject.consume(defaultArgs.copy(help = true))
	}

	fun picoCliPath() {
		val subject = MoodleTaskApp()
		subject.consume(defaultArgs.copy(help = true).typed())
	}
}
