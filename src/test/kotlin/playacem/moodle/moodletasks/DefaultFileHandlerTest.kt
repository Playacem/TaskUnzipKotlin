package playacem.moodle.moodletasks

import org.testng.Assert.assertNull
import org.testng.Assert.assertTrue
import org.testng.annotations.Test
import playacem.moodle.util.fileEventGeneric


@Test
class DefaultFileHandlerTest {
	fun defaultNext() {
		assertNull(FileHandler.defaultHandler.next, "Next should be null")
	}

	fun defaultApply() {
		assertTrue(FileHandler.defaultHandler.apply(fileEventGeneric), "Apply should always return true")
	}

	/**
	 * Tests if there are any exceptions
	 */
	fun defaultAction() {
		FileHandler.defaultHandler.action(fileEventGeneric)
		assertTrue(true)
	}

	fun createdNext() {
		assertNull(FileHandler.DefaultHandler().next, "Next should be null")
	}

	fun createdApply() {
		assertTrue(FileHandler.DefaultHandler().apply(fileEventGeneric), "Apply should always return true")
	}

	/**
	 * Tests if there are any exceptions
	 */
	fun createdAction() {
		FileHandler.DefaultHandler().action(fileEventGeneric)
		assertTrue(true)
	}
}
