package playacem.moodle.moodletasks

import org.testng.Assert.assertNotNull
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.api.FileEventHandler
import playacem.moodle.util.DataArray
import playacem.moodle.util.and
import playacem.moodle.util.handlerResult
import playacem.moodle.util.unaryPlus


@Test
class FileHandlerChainTest {
	fun emptyArgs() {
		val actual = FileHandler.createChain()
		assertNotNull(actual as? FileHandler.DefaultHandler)
	}

	@DataProvider(name = "args-default")
	fun createDefaultHandlerData(): DataArray {
		return arrayOf(
			+FileHandler.DefaultHandler(),
			FileHandler.DefaultHandler() and FileHandler.DefaultHandler(),
			FileHandler.DefaultHandler() and FileHandler.ZipHandler(),
			FileHandler.DefaultHandler() and FileHandler.ProjectFileHandler()
		)
	}

	@Test(dataProvider = "args-default")
	fun defaultHandlerResult(handlers: Array<FileEventHandler>) {
		handlerResult<FileHandler.DefaultHandler>(handlers)
	}

	@DataProvider(name = "args-zip")
	fun createZipHandlerData(): DataArray {
		return arrayOf(
			+FileHandler.ZipHandler(),
			FileHandler.ZipHandler() and FileHandler.DefaultHandler(),
			FileHandler.ZipHandler() and FileHandler.ZipHandler(),
			FileHandler.ZipHandler() and FileHandler.ProjectFileHandler()
		)
	}

	@Test(dataProvider = "args-zip")
	fun zipHandlerResult(handlers: Array<FileEventHandler>) {
		handlerResult<FileHandler.ZipHandler>(handlers)
	}

	@DataProvider(name = "args-project-file")
	fun createProjectFileHandlerData(): DataArray {
		return arrayOf(
			+FileHandler.ProjectFileHandler(),
			FileHandler.ProjectFileHandler() and FileHandler.DefaultHandler(),
			FileHandler.ProjectFileHandler() and FileHandler.ZipHandler(),
			FileHandler.ProjectFileHandler() and FileHandler.ProjectFileHandler()
		)
	}

	@Test(dataProvider = "args-project-file")
	fun projectFileHandlerResult(handlers: Array<FileEventHandler>) {
		handlerResult<FileHandler.ProjectFileHandler>(handlers)
	}
}
