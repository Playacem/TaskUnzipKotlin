package playacem.moodle.moodletasks

import org.testng.Assert.*
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeGroups
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.extensions.toPath
import playacem.moodle.moodletasks.Status.*
import playacem.moodle.util.DataArray
import playacem.moodle.util.unaryPlus


data class SampleParameters(val n: Int, val s: String = "a")
data class StatusSample(val status: Status, val sample: SampleParameters)

private fun params(status: Status, n: Int, s: String = "a") =
	StatusSample(status, SampleParameters(n, s))

private const val DISABLED = "stat-disabled"
private const val DISABLED_KEY = "$DISABLED-data"
private const val DISABLED_PATH = "$DISABLED-path"

private const val ENABLED = "stat-enabled"
private const val ENABLED_KEY = "$ENABLED-data"
private const val ENABLED_PATH = "$ENABLED-path"

@Test
class StatisticsTest {
	private val noInputParam = params(NO_INPUT_DETECTED, 2, "b no-input")
	private val noZipParam = params(NO_ZIP_DETECTED, 1, "d no-zip")
	private val noEclipseParam = params(NO_ECLIPSE_PROJECT, 5, "c no-eclipse")
	private val validParam = params(VALID, 3, "a valid")

	@BeforeGroups(DISABLED, DISABLED_PATH)
	fun off() {
		Statistics.enabled = false
	}

	@BeforeGroups(ENABLED, ENABLED_PATH)
	fun on() {
		Statistics.enabled = true
	}

	@AfterMethod
	fun teardown() {
		Statistics.reset()
	}

	private fun createSample(par: SampleParameters): List<String> {
		val (n, s) = par
		val list = mutableListOf<String>()
		val array = arrayOfNulls<String>(n)
		for (i in array.indices) {
			list += (s + s.repeat(i))
		}
		return list
	}

	private fun setupSingle(statusSample: StatusSample) {
		val sampleUsers = createSample(statusSample.sample)
		when (statusSample.status) {
			NO_INPUT_DETECTED -> sampleUsers.forEach(Statistics::init)
			NO_ZIP_DETECTED -> sampleUsers.forEach(Statistics::noZipDetected)
			NO_ECLIPSE_PROJECT -> sampleUsers.forEach(Statistics::noEclipseProject)
			VALID -> sampleUsers.forEach(Statistics::valid)
		}
	}

	private fun setupMultiple(statusSample: StatusSample) {
		val sampleUsers = createSample(statusSample.sample)
		when (statusSample.status) {
			NO_INPUT_DETECTED -> Statistics.init(sampleUsers)
			NO_ZIP_DETECTED -> Statistics.noZipDetected(sampleUsers)
			NO_ECLIPSE_PROJECT -> Statistics.noEclipseProject(sampleUsers)
			VALID -> Statistics.valid(sampleUsers)
		}
	}


	@DataProvider(name = DISABLED_KEY)
	fun disabledCaseData(): DataArray {
		return arrayOf(
			+noInputParam,
			+noZipParam,
			+noEclipseParam,
			+validParam
		)
	}

	@DataProvider(name = ENABLED_KEY)
	fun enabledCaseData(): DataArray {
		return arrayOf(
			+setOf(noInputParam),
			+setOf(noZipParam),
			+setOf(noEclipseParam),
			+setOf(validParam),

			+setOf(noInputParam, noZipParam),
			+setOf(noInputParam, noEclipseParam),
			+setOf(noInputParam, validParam),

			+setOf(noZipParam, noEclipseParam),
			+setOf(noZipParam, validParam),

			+setOf(noEclipseParam, validParam),

			+setOf(noInputParam, noZipParam, noEclipseParam),
			+setOf(noInputParam, noZipParam, validParam),
			+setOf(noInputParam, noEclipseParam, validParam),
			+setOf(noZipParam, noEclipseParam, validParam),

			+setOf(noInputParam, noZipParam, noEclipseParam, validParam)
		)
	}

	@Test(groups = [DISABLED], dataProvider = DISABLED_KEY)
	fun disabledCaseSingle(input: StatusSample) {
		setupSingle(input)
		assertNull(Statistics.generateReport())
	}

	@Test(groups = [DISABLED], dataProvider = DISABLED_KEY)
	fun disabledCaseMultiple(input: StatusSample) {
		setupMultiple(input)
		assertNull(Statistics.generateReport())
	}

	private fun doEnabled(inputs: Set<StatusSample>) {
		val actual = Statistics.generateReport() ?: throw AssertionError("Report is null")
		val actualLines = actual.split("\n")
		inputs.forEach { (status, sample) ->
			with(status) {
				assertTrue(categoryName in actual)
				assertTrue(description in actual)
			}

			with(sample) {
				val currentActual = actualLines.filter { it.startsWith(s) }.size
				assertEquals(currentActual, n)
			}
		}
	}

	@Test(groups = [ENABLED], dataProvider = ENABLED_KEY)
	fun enabledCaseSingle(inputs: Set<StatusSample>) {
		inputs.forEach { setupSingle(it) }
		doEnabled(inputs)
	}

	@Test(groups = [ENABLED], dataProvider = ENABLED_KEY)
	fun enabledCaseMultiple(inputs: Set<StatusSample>) {
		inputs.forEach { setupMultiple(it) }
		doEnabled(inputs)
	}

	private val invalidPath = "_BLA".toPath()
	private val validPath = "USER_BLA".toPath()

	@Test(groups = [DISABLED_PATH])
	fun disabledNoZipInvalidPath() {
		Statistics.noZipDetected(invalidPath)
		assertNull(Statistics.generateReport())
	}

	@Test(groups = [DISABLED_PATH])
	fun disabledNoZipValidPath() {
		Statistics.noZipDetected(validPath)
		assertNull(Statistics.generateReport())
	}

	@Test(groups = [ENABLED_PATH])
	fun enabledNoZipInvalidPath() {
		Statistics.noZipDetected(invalidPath)
		val actual = Statistics.generateReport()
		assertEquals(actual, "")
	}

	@Test(groups = [ENABLED_PATH])
	fun enabledNoZipValidPath() {
		Statistics.noZipDetected(validPath)
		val actual = Statistics.generateReport() ?: throw AssertionError("Report is null")
		val actualLines = actual.split("\n")
		val user = userStringOfPath(validPath)
		with(NO_ZIP_DETECTED) {
			assertTrue(categoryName in actual)
			assertTrue(description in actual)
		}
		assertNotNull(user)
		if (user != null) {
			assertTrue(user in actual)
			val size = actualLines.filter { it.startsWith(user) }.size
			assertEquals(size, 1)
		}
	}
}
