package playacem.moodle.moodletasks

import org.testng.Assert.assertEquals
import org.testng.Assert.assertTrue
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.extensions.bufferedReader
import playacem.moodle.extensions.div
import playacem.moodle.util.DataArray
import playacem.moodle.util.ResourceFolder
import playacem.moodle.util.and
import java.nio.file.Files
import java.nio.file.Path

@Test
class ChangeProjectNameTest {
	private val dir = ResourceFolder.SELF / "projectName"
	private val invalidXml = dir / "invalid.project.xml"
	private val invalidBackup = dir / "${invalidXml.fileName}.bak"
	private val validXml = dir / "valid.project.xml"
	private val validBackup = dir / "${validXml.fileName}.bak"

	private val nonExistentPath = dir / "not-found"

	@BeforeMethod
	fun setup() {
		Files.copy(invalidXml, invalidBackup)
		Files.copy(validXml, validBackup)
	}

	@AfterMethod
	fun teardown() {
		Files.deleteIfExists(invalidXml)
		Files.deleteIfExists(validXml)

		Files.move(invalidBackup, invalidXml)
		Files.move(validBackup, validXml)
	}

	private fun verifyChange(target: String?) {
		fun Path.contentAsString() = this.bufferedReader().useLines { it.joinToString() }
		val original = validXml.contentAsString()
		val backup = validBackup.contentAsString()
		if (target == null) {
			throw AssertionError("target is null")
		}
		assertTrue(target in original)
		assertTrue(target !in backup)
	}

	private fun verifyNoChange() {
		fun Path.linesToList() = this.bufferedReader().useLines {
			it.filter(String::isNotBlank)
				.toList()
		}

		val invalidLines = invalidXml.linesToList()
		val invalidBackupLines = invalidBackup.linesToList()

		assertEquals(invalidLines, invalidBackupLines)

		val validLines = validXml.linesToList()
		val validBackupLines = validBackup.linesToList()

		assertEquals(validLines, validBackupLines)
	}

	@DataProvider(name = "path-data")
	fun pathData(): DataArray {
		return arrayOf(
			validXml and "magic string",
			validXml and "777"
		)
	}

	@DataProvider(name = "path-data-noop")
	fun pathDataNoop(): DataArray {
		return arrayOf(
			validXml and null,
			invalidXml and "invalid",
			invalidXml and null,
			nonExistentPath and "???",
			nonExistentPath and null
		)
	}

	@DataProvider(name = "string-data")
	fun stringData(): DataArray {
		return pathData().map { arrayOf(it[0].toString(), it[1]) }.toTypedArray()
	}

	@DataProvider(name = "string-data-noop")
	fun stringDataNoop(): DataArray {
		return pathDataNoop().map { arrayOf(it[0].toString(), it[1]) }.toTypedArray()
	}

	@DataProvider(name = "path-data-simple")
	fun pathDataSimple(): DataArray {
		return pathData().map { arrayOf(it[0]) }.toTypedArray()
	}

	@DataProvider(name = "path-data-simple-noop")
	fun pathDataSimpleNoop(): DataArray {
		return pathDataNoop().map { arrayOf(it[0]) }.toTypedArray()
	}

	@Test(dataProvider = "string-data")
	fun stringBased(project: String, userString: String?) {
		changeProjectName(project = project, userString = userString)
		verifyChange(userString)
	}

	@Test(dataProvider = "string-data")
	fun testFlag(project: String, userString: String?) {
		changeProjectName(project = project, userString = userString, newImpl = true)
		verifyChange(userString)
	}

	@Test(dataProvider = "path-data")
	fun pathBased(project: Path, userString: String?) {
		changeProjectName(project = project, userString = userString)
		verifyChange(userString)
	}

	@Test(dataProvider = "path-data-simple")
	fun pathBasedWithDefault(project: Path) {
		changeProjectName(project = project)
		verifyNoChange() // no valid username in path
	}

	@Test(dataProvider = "string-data-noop")
	fun stringBasedNoChange(project: String, userString: String?) {
		changeProjectName(project = project, userString = userString)
		verifyNoChange()
	}

	@Test(dataProvider = "string-data-noop")
	fun testFlagNoChange(project: String, userString: String?) {
		changeProjectName(project = project, userString = userString, newImpl = true)
		verifyNoChange()
	}

	@Test(dataProvider = "path-data-noop")
	fun pathBasedNoChange(project: Path, userString: String?) {
		changeProjectName(project = project, userString = userString)
		verifyNoChange()
	}

	@Test(dataProvider = "path-data-simple-noop")
	fun pathBasedWithDefaultNoChange(project: Path) {
		changeProjectName(project = project)
		verifyNoChange()
	}
}
