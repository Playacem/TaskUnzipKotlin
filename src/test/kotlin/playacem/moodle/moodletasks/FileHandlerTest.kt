package playacem.moodle.moodletasks

import org.testng.Assert.assertTrue
import org.testng.annotations.AfterMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.api.FileEvent
import playacem.moodle.util.DataArray
import playacem.moodle.util.fileEventGeneric
import playacem.moodle.util.fileEventWithUser
import playacem.moodle.util.unaryPlus

@Test
class FileHandlerTest {

	@DataProvider(name = "events")
	fun fileEventsProvider(): DataArray {
		return arrayOf(
			+(fileEventGeneric),
			+(fileEventWithUser)
		)
	}

	/**
	 * No exceptions should be thrown
	 */
	@Test(dataProvider = "events")
	fun defaultHandle(event: FileEvent) {
		FileHandler.defaultHandler.handle(event)
		assertTrue(true)
	}

	/**
	 * Covers the else part of when statement
	 */
	@Test(dataProvider = "events")
	fun defaultHandleWithElsePath(event: FileEvent) {
		FileHandler.ZipHandler().handle(event)
		assertTrue(true)
	}

	/**
	 * No exceptions should be thrown
	 */
	@Test(dataProvider = "events")
	fun defaultZipChain(event: FileEvent) {
		FileHandler.handleOnlyZips(event)
		assertTrue(true)
	}

	/**
	 * No exceptions should be thrown
	 */
	@Test(dataProvider = "events")
	fun defaultNonZipChain(event: FileEvent) {
		FileHandler.handleNonZips(event)
		assertTrue(true)
	}

	@AfterMethod
	fun tearDown() {
		Statistics.reset()
	}
}
