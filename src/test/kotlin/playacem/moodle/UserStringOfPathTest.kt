package playacem.moodle

import org.testng.Assert.assertEquals
import org.testng.Assert.assertNull
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.extensions.div
import playacem.moodle.extensions.toPath
import playacem.moodle.moodletasks.userStringOfPath
import playacem.moodle.util.DataArray
import playacem.moodle.util.and
import playacem.moodle.util.unaryPlus
import java.nio.file.Path

@Test
class UserStringOfPathTest {


	@DataProvider(name = "userless")
	fun createUserlessData(): DataArray {
		return arrayOf(
			+("bla".toPath()),
			+("bla" / "bla"),
			+("_bla".toPath() / "_buhuu" / "_hi"),
			+("/bla".toPath()),
			+("_".toPath()),
			+("_hi".toPath()),
			+("/_test".toPath())
		)
	}

	@DataProvider(name = "user-string")
	fun createUserStringData(): DataArray {
		return arrayOf(
			"USER_bla".toPath() and "USER",
			("/" / "Name_bla") and "Name",
			("ooo" / "Hi_bla") and "Hi",
			("ooo" / "Space name_007") and "Space name"
		)
	}

	@Test(dataProvider = "userless")
	fun userlessString(path: Path) {
		val actual = userStringOfPath(path)
		assertNull(actual)
	}

	@Test(dataProvider = "user-string")
	fun userString(path: Path, expected: String) {
		val actual = userStringOfPath(path)
		assertEquals(actual, expected)
	}
}
