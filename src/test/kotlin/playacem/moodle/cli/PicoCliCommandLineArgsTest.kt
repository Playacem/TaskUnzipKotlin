package playacem.moodle.cli

import org.testng.Assert.assertEquals
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.extensions.toPath
import playacem.moodle.util.DataArray
import playacem.moodle.util.and

@Test
class PicoCliCommandLineArgsTest {
	private val defaultArgs = TypedArguments(
		inputFolder = ".".toPath(),
		list = null,
		targetFolder = "output".toPath(),
		encoding = "UTF-8",
		clean = false,
		showStatistics = false,
		help = false
	)

	fun testNoArgs() {
		val actual = parseArgsTyped(emptyArray())
		assertEquals(actual, defaultArgs.copy(help = true))
	}

	fun testShortArgumentNames() {
		val actual = parseArgsTyped(arrayOf("-c", "-e", "UTF-8", "-l", "list", "-h"))
		val expected = defaultArgs.copy(clean = true, encoding = "UTF-8", list = "list".toPath(), help = true)
		assertEquals(actual, expected)
	}

	fun testLongArgumentNames() {
		val actual =
			parseArgsTyped(arrayOf("--clean", "--encoding", "UTF-8", "--list", "list", "--help"))
		val expected = defaultArgs.copy(clean = true, encoding = "UTF-8", list = "list".toPath(), help = true)
		assertEquals(actual, expected)
	}

	@DataProvider(name = "io-keys")
	fun createInOutData(): DataArray = arrayOf(
		"-i" and "-o",
		"-i" and "--out",
		"-i" and "--output",
		"--in" and "-o",
		"--in" and "--out",
		"--in" and "--output",
		"--input" and "-o",
		"--input" and "--out",
		"--input" and "--output"
	)

	@Test(dataProvider = "io-keys")
	fun testInOutArguments(inKey: String, outKey: String) {
		val actual = parseArgsTyped(arrayOf(inKey, "inputFolder", outKey, "outputFolder"))
		val expected = defaultArgs.copy(inputFolder = "inputFolder".toPath(), targetFolder = "outputFolder".toPath())
		assertEquals(actual, expected)
	}

	fun testShortFlagNames() {
		val actual = parseArgsTyped(arrayOf("-c", "-h"))
		val expected = defaultArgs.copy(clean = true, help = true)
		assertEquals(actual, expected)
	}

	fun testLongFlagNames() {
		val actual = parseArgsTyped(arrayOf("--clean", "--show-stats", "--help"))
		val expected = defaultArgs.copy(clean = true, showStatistics = true, help = true)
		assertEquals(actual, expected)
	}
}
