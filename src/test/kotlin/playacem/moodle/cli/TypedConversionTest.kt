package playacem.moodle.cli

import org.testng.Assert.assertEquals
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.extensions.toPath
import playacem.moodle.util.DataArray
import playacem.moodle.util.and

class TypedConversionTest {
	private val jCommanderArgs = Arguments(
		inputFolder = ".",
		list = null,
		targetFolder = "output",
		encoding = "UTF-8",
		clean = false,
		showStatistics = false,
		help = false
	)

	private val picoCliArgs = TypedArguments(
		inputFolder = ".".toPath(),
		list = null,
		targetFolder = "output".toPath(),
		encoding = "UTF-8",
		clean = false,
		showStatistics = false,
		help = false
	)

	@DataProvider(name = "args")
	fun createArgs(): DataArray {
		return arrayOf(
			jCommanderArgs and picoCliArgs,
			jCommanderArgs.copy(list = "test") and picoCliArgs.copy(list = "test".toPath())
		)
	}

	@Test(dataProvider = "args")
	fun testTypedExtension(oldArgs: Arguments, typedArgs: TypedArguments) {
		assertEquals(oldArgs.typed(), typedArgs, "typed conversion does not work")
	}
}
