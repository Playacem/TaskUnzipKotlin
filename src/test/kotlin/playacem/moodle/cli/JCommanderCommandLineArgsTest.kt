package playacem.moodle.cli

import org.testng.Assert.assertEquals
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.util.DataArray
import playacem.moodle.util.and

@Test
class JCommanderCommandLineArgsTest {
	private val defaultArgs = Arguments(
		inputFolder = ".",
		list = null,
		targetFolder = "output",
		encoding = "UTF-8",
		clean = false,
		showStatistics = false,
		help = false
	)

	fun testNoArgs() {
		val actual = parseArgs(emptyArray())
		assertEquals(actual, defaultArgs.copy(help = true))
	}

	fun testShortArgumentNames() {
		val actual = parseArgs(arrayOf("-c", "-e", "UTF-8", "-l", "list", "-h"))
		val expected = defaultArgs.copy(clean = true, encoding = "UTF-8", list = "list", help = true)
		assertEquals(actual, expected)
	}

	fun testLongArgumentNames() {
		val actual =
			parseArgs(arrayOf("--clean", "--encoding", "UTF-8", "--list", "list", "--help"))
		val expected = defaultArgs.copy(clean = true, encoding = "UTF-8", list = "list", help = true)
		assertEquals(actual, expected)
	}

	@DataProvider(name = "io-keys")
	fun createInOutData(): DataArray = arrayOf(
		"-i" and "-o",
		"-i" and "--out",
		"-i" and "--output",
		"--in" and "-o",
		"--in" and "--out",
		"--in" and "--output",
		"--input" and "-o",
		"--input" and "--out",
		"--input" and "--output"
	)

	@Test(dataProvider = "io-keys")
	fun testInOutArguments(inKey: String, outKey: String) {
		val actual = parseArgs(arrayOf(inKey, "inputFolder", outKey, "outputFolder"))
		val expected = defaultArgs.copy(inputFolder = "inputFolder", targetFolder = "outputFolder")
		assertEquals(actual, expected)
	}

	fun testShortFlagNames() {
		val actual = parseArgs(arrayOf("-c", "-h"))
		val expected = defaultArgs.copy(clean = true, help = true)
		assertEquals(actual, expected)
	}

	fun testLongFlagNames() {
		val actual = parseArgs(arrayOf("--clean", "--show-stats", "--help"))
		val expected = defaultArgs.copy(clean = true, showStatistics = true, help = true)
		assertEquals(actual, expected)
	}
}
