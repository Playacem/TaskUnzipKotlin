package playacem.moodle

import org.testng.Assert.assertFalse
import org.testng.Assert.assertTrue
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.extensions.div
import playacem.moodle.extensions.exists
import playacem.moodle.util.DataArray
import playacem.moodle.util.ResourceFolder
import playacem.moodle.util.unaryPlus
import java.nio.file.Path

@Test
class MainRealUsage {
	@Test(dataProvider = "pro")
	fun realUsage(parameter: RealUsageParameter) {
		main(parameter.toArgsArray())
		val out = parameter.outDir

		assertTrue(out.exists)
		deleteRecursively(out)
		assertFalse(out.exists)
	}

	@DataProvider(name = "pro")
	fun dataProvider(): DataArray {
		val defaultParam = RealUsageParameter()
		return arrayOf(
			+defaultParam,
			+defaultParam.copy(studentFile = defaultParam.base / "notfound.txt"),
			+defaultParam.copy(studentFile = null),
			+defaultParam.copy(encoding = "ASCII"),
			+defaultParam.copy(encoding = "ASCII", showStats = false)
		)
	}
}

data class RealUsageParameter(
	val base: Path = ResourceFolder.SELF / "moodle-export",
	val studentFile: Path? = base / "students.txt",
	val outDir: Path = base / "output",
	val encoding: String = "UTF-8",
	val cleanFlag: Boolean = true,
	val showStats: Boolean = true
)

fun RealUsageParameter.toArgsArray(): Array<String> {
	val list = mutableListOf(
		"-i",
		base.toString(),
		"-o",
		outDir.toString(),
		"--encoding",
		encoding
	)
	if (studentFile != null) {
		list.add("--list")
		list.add(studentFile.toString())
	}
	if (cleanFlag) {
		list.add("-c")
	}
	if (showStats) {
		list.add("--show-stats")
	}

	return list.toTypedArray()
}
