package playacem.moodle

import org.testng.Assert.assertEquals
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import playacem.moodle.util.DataArray
import playacem.moodle.util.and
import playacem.moodle.util.unaryPlus
import java.io.File

class TrimFileNameTest {

	private val sep: String = File.separator

	@DataProvider(name = "not-affected")
	fun createNotAffected(): DataArray {
		return arrayOf(
			+"",
			+"This",
			+"Hello World",
			+"Extra     Space",
			+"Hi"
		)
	}

	@DataProvider(name = "affected")
	fun createAffected(): DataArray {
		return arrayOf(
			"""\""" and sep,
			""" \""" and sep,
			""" \ """ and sep,
			"""/""" and sep,
			""" /""" and sep,
			""" / """ and sep,
			"""Whitespace   """ and "Whitespace",
			"""   Left""" and "Left",
			"""    Padded    """ and "Padded",
			"""A:\WinDir\""" and "A:${sep}WinDir$sep",
			"""A:\File.txt""" and "A:${sep}File.txt",
			"""file.txt  """ and "file.txt",
			"""/root/fake/dir/""" and "${sep}root${sep}fake${sep}dir$sep",
			"""/root/file.txt""" and "${sep}root${sep}file.txt",
			"""path/to/dir  """ and "path${sep}to${sep}dir"

		)
	}

	@Test(dataProvider = "not-affected")
	fun notAffected(fileName: String) {
		val actual = trimFileName(fileName)
		assertEquals(actual, fileName)
	}

	@Test(dataProvider = "affected")
	fun affected(fileName: String, expected: String) {
		val actual = trimFileName(fileName)
		assertEquals(actual, expected)
	}
}
