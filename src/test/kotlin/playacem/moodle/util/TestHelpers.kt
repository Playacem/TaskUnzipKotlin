@file:Suppress("UNUSED")

package playacem.moodle.util

import playacem.moodle.extensions.div

object ResourceFolder {
	val SELF = "src" / "test" / "resources"
}

operator fun Any?.unaryPlus(): Array<Any?> = arrayOf(this)

infix fun Any?.and(other: Any?): Array<Any?> = arrayOf(this, other)
infix fun Any?.and(other: Nothing?): Array<Any?> = arrayOf(this, other)
infix fun Any?.and(others: Array<Any?>): Array<Any?> = arrayOf(this, *others)

infix fun Array<Any?>.and(other: Any?): Array<Any?> = arrayOf(*this, other)
infix fun Array<Any?>.and(other: Nothing?): Array<Any?> = arrayOf(*this, other)
infix fun Array<Any?>.and(others: Array<Any?>): Array<Any?> = arrayOf(*this, *others)

@Deprecated("use DataArray instead", replaceWith = ReplaceWith("DataArray", "playacem.moodle.util.DataArray"))
typealias Array2<T> = Array<Array<T>>

typealias DataArray = Array<Array<Any?>>
