package playacem.moodle.util

import org.testng.Assert
import playacem.moodle.api.FileEvent
import playacem.moodle.api.FileEventHandler
import playacem.moodle.extensions.toPath
import playacem.moodle.moodletasks.FileHandler

val fileEventGeneric = FileEvent("/".toPath())
val fileEventWithUser = FileEvent("/".toPath(), user = "Hello")

inline fun <reified T> handlerResult(handlers: Array<FileEventHandler>) {
	val actual = FileHandler.createChain(*handlers)
	Assert.assertNotNull(actual as? T)
	Assert.assertEquals(actual, handlers[0])
}
