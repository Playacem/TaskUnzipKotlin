# TaskUnzip Kotlin - Mass Extract Eclipse Projects from Moodle Tasks

This tool enables automated extracting and renaming of Eclipse projects inside a zip file.

## Building the application

This segment describes how to build the application for release.
The application currently requires Java 8 configured as a Gradle toolchain to build. 

The [Gradle docs for auto-detection](https://docs.gradle.org/8.0.2/userguide/toolchains.html#sec:auto_detection) mention [IntelliJ IDEA installations](https://www.jetbrains.com/help/idea/sdk.html#set-up-jdk) as a possible source.
This is the desired way to set this up.

### Building with Gradle

Simply execute `./gradlew shadowJar`.
The result will be in the `./build/libs` folder.

The jar will be named `TaskUnzipKotlin.jar`

Other useful commands:

- `./gradlew clean` to clean the output folder
- `./gradlew test` to run the unit tests
- `./gradlew shadowJar` to generate a fat jar

### Updating Gradle

Run `./gradlew help --scan` first to check out a build scan for deprecations also helps with updates.

Run `./gradlew wrapper --gradle-version 8.0.2` with the new target version.

### Updating dependencies

Dependencies are managed with the excellent refreshVersions plugin, historically with buildSrc parts.
For new libraries run `./gradlew buildSrcLibs`
and run `./gradlew refreshVersions` for version updates.

Cleaning up the versions.properties file is done via `./gradlew refreshVersionsCleanup`.

## Workflow

- Download a zip with all the handed in exercises from Moodle.
- Run the TaskUnzip tool with the commandline options you want.
	- Use `--input` to specify the folder containing the zip file
	- Use `--output` to customize the output folder
	- Use `--list` to specify a file with all the students in a particular lesson
	- Use `--show-stats` for a report at the end of the process
- Import into Eclipse by using the import option `Import Existing Projects into Workspace`
  (Under `File->Import...->General`) and navigating to the output folder created by the tool.

## Example usage

```
java -jar <jarname> --list students.txt --output "Monday-0800" --show-stats
```

## Usage

For an up-to-date version run `java -jar <jarname> --help`

```
Usage: TaskUnzip [options]
  Options:
    -c, --clean
      Clean output folder before running.
      Default: false
    -e, --encoding
      Encoding that is used to treat files.
      Default: UTF-8
    -i, --in, --input, -f, --folder
      The folder to be searched for zip files. Defaults to current directory.
      Default: .
    -l, --list
      The name of the file to be used in the filtering phase. The file should
      contain one user per line. This is required if you want to use
      --show-stats
    -o, --out, --output
      The folder to be used for the generated output.
      Default: output
    --show-stats
      Display statistics after running the program. You need to specify a list
      of users with --list for this option to work.
      Default: false
    -h, --help, -?, --usage
      Prints this help text
```
