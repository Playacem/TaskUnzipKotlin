/*
 * This file was generated by the Gradle 'init' task.
 *
 * The settings file is used to specify which projects to include in your build.
 *
 * Detailed information about configuring a multi-project build in Gradle can be found
 * in the user guide at https://docs.gradle.org/4.10.2/userguide/multi_project_builds.html
 */

pluginManagement {
	repositories {
		gradlePluginPortal()
		mavenCentral()
	}
}

plugins {
	// See https://jmfayard.github.io/refreshVersions
	id("de.fayard.refreshVersions") version "0.60.5"
	id("org.gradle.toolchains.foojay-resolver-convention") version "0.8.0"
}

refreshVersions {
	enableBuildSrcLibs()
}

rootProject.name = "TaskUnzipKotlin"
