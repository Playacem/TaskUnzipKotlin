import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
	application
	kotlin("jvm") version "_"
	id("com.github.johnrengelman.shadow") version "_"
}

repositories {
	mavenCentral()
}

dependencies {
	implementation(kotlin("stdlib-jdk8"))
	implementation(Libs.jcommander)
	implementation(Libs.picocli)
	testImplementation(Libs.testng)
}

application {
	group = "playacem.moodle"
	version = App.VERSION
	applicationName = "TaskUnzipKotlin"
	mainClass.set("playacem.moodle.MainKt")
}

kotlin {
	jvmToolchain(21)
}

tasks {
	withType<Test> {
		useTestNG(closureOf<TestNGOptions> {
			suites("src/test/resources/testng.xml")
		})
	}
	withType<Wrapper> {
		distributionType = Wrapper.DistributionType.ALL
	}
	withType<ShadowJar> {
		archiveVersion.set("")
		archiveClassifier.set("")
	}
}
